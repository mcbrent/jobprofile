<div class="wrapper wrapper-content animated fadeInLeft">
  <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-info float-e-margins">
          <div class="panel-heading">
            <h5>Attendance</h5>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">

              </div>
              <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Job Fair Attendance - Applicant</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                      <div class="table-responsive">
                        <table id="applicant-list" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-3 text-center">Applicant</th>
                              <th class="col-md-2 text-center">Gender / Age</th>
                              <th class="col-md-1 text-center">Email</th>
                              <th class="col-md-1 text-center">Category</th>
                              <th class="col-md-2 text-center">Contact</th>
                              <th class="col-md-2 text-center">Address</th>
                              <th class="col-md-1 text-center no-sort">Attendance</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                      </div>

                    </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Job Fair Attendance - Employer</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                      <div class="table-responsive">
                        <table id="employer-list" class="table table-striped table-bordered table-hover" data-config="{}">
                          <thead>
                            <tr>
                              <th class="col-md-4 text-center">Employer Name</th>
                              <th class="col-md-3 text-center">Address</th>
                              <th class="col-md-2 text-center">Email</th>
                              <th class="col-md-2 text-center">Contact</th>
                              <th class="col-md-2 text-center"></th>
                              <th class="col-md-2 text-center"></th>
                              <th class="col-md-1 text-center no-sort">Attendance </th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                      </div>

                    </div>
                </div>

              </div>
            </div>
          </div>
        </div>


      </div>

    </div>

</div>

<script src="<?php echo JS_DIR ?>components/utility/jobfair_list.js"></script>
